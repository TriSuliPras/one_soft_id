<?php

namespace App\Core;

class View 
{
	public static $component;

	public static function HTML_BEGIN()
	{
		echo '
			<!DOCTYPE html>
			<html lang="en">
			 	<head>
					<meta charset="UTF-8"/>
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
					<meta name="viewport" content="width=device-width, initial-scale=1"/>
					<title>Train</title>
					<meta name="description" content=""/>
					<link rel="stylesheet" href="http://onesoft.local/styles/plugins.css"/>
					<link rel="stylesheet" href="http://onesoft.local/styles/main.css"/>
			 	</head>
		';	
	}

	public static function BODY_BEGIN()
	{
		echo '
				<body class="projects">
			  		<div id="wrap">
		';
	}

	public static function BODY_END()
	{
		echo '
					<footer>
				        <div class="content-wrap">
				        	Copyright &copy; 2018. Inspirasi Digital Eksperiensia.
				        </div>
				     </footer>
				    </div>
			  	</body>
		';
	}

	public static function INCLUDE($page)
	{
		require_once '../app/Views/layouts/'. $page .'.php';
	}

	public static function HTML_END()
	{
		echo '
					
				<script src="http://onesoft.local/plugins/jquery-3.3.1/jquery-3.3.1.min.js"></script>
				<script src="http://onesoft.local/plugins/bootstrap-4/js/bootstrap.bundle.min.js"></script>
				<script src="http://onesoft.local/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
				<script src="http://onesoft.local/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
				<script src="http://onesoft.local/scripts/main.js"></script>
			</html>

		';	
	}

	public static function MAKE($comp)
	{
		self::$component = $comp;
		
		return $this;
	}

	public static function ADDCHILD($comp, $numbs)
	{
		
	}

	public static function RENDER()
	{
		return ;
	}
}