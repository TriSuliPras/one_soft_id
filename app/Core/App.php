<?php 

class App 
{
	/**
     * Default Controller
     * * @var string
     */
	protected $controller = "IndexController";
	
	/**
     * Default Method in the Controller
     * @var string
     */
	protected $method = "index";
	
	/**
     * Specify the params to be empty array by default
     * @var array 
     */
	protected $params = [];

	public function __construct()
	{
		$url = $this->parseUrl();

		$_DIR_NAME = '../app/Controllers/';

		$CONTROLLER = 'controller';
		
		/**
		 * Check if controller exist
	     */
		if (file_exists($_DIR_NAME. $url[0].$CONTROLLER .'.php'))
		{
			$this->controller = ucfirst($url[0]).ucfirst($CONTROLLER);

			unset($url[0]);
		}
		
		require_once $_DIR_NAME. $this->controller. '.php';

		$this->controller = new $this->controller;
		
		/**
		 * Check if method exist
	     */
		if (isset($url[1]))
		{
			if (method_exists($this->controller, $url[1])) {

				$this->method = $url[1];

				unset($url[1]);
			}	
		}
		
		/**
		 * Check if params exist
	     */
		$this->params = $url ? array_values($url) : [];
		
		
		/**
		 * this will containing the controller 
		 * and the method from it self 
		 * and the second arg will be the params 
		 * would be to pass through
	     */
		call_user_func_array([$this->controller, $this->method], $this->params);
	}


	protected function parseUrl()
	{
		if (isset($_GET['url']))
		{
			$url = explode('/', filter_var( rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL ) );

			return $url;
		}
	}
}