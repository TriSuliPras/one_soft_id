<div class="row">
    <div class="col-md-10 left-wrap">
        <div class="image" style="background-image: url(images/login-bg.jpg);"></div>
    </div>
    <div class="col-md-2 right-wrap">
        <div class="form-wrap">
            <div class="box">
                <h1 class="logo"><img src="images/harnods-logo.svg"/>Fast Travel</h1>
                <p>Pleaese sign in to continue</p>
                <form action="train/station" method="POST">
                    <div class="form-group empw">
                        <input class="form-control top"
                               type="text"
                               name="outletId"
                               placeholder="ID Outlet" />
                        <input class="form-control bot"
                               type="text"
                               name="pin"
                               placeholder="Pin"/>
                        <select name="key" class="select-control" title="Key">
                            <option value="FASTPAY">Fastpay</option>
                        </select>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary btn-block" type="submit">
                                Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>