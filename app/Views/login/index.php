<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title></title>
        <meta name="description" content=""/>
        <link rel="stylesheet" href="styles/plugins.css"/>
        <link rel="stylesheet" href="styles/main.css"/>
    </head>
    <body class="login">
        <div id="wrap">
            <main>
                <div class="wrap">
                    <div class="section">
                        <div class="container-fluid auth-page">
                            <?php include "form.php"; ?>
                        </div>
                    </div>
                    <div class="mask"></div>
                </div>
            </main>
        </div>
    </body>
    <script src="plugins/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="plugins/bootstrap-4/js/bootstrap.bundle.min.js"></script>
    <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="scripts/main.js"></script>
</html>