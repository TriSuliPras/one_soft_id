<header>
    <h1 class="logo">
        <a href="#">Fast Travel</a>
    </h1>
    <div class="dropdown">
        <div class="account" data-toggle="dropdown">
            <div class="image"><img src="http://onesoft.local/images/profile-pic.jpg"/></div>
            <div class="content">
                <span class="name">Vinna Luthfiana</span>
                <span class="role">Administrator</span>
            </div>
        </div>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Logout</a>
        </div>
    </div>
</header>