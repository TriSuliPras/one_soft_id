<?php use App\Core\View; ?>

<?php View::HTML_BEGIN();?>

	<?php View::INCLUDE('header');?>

	<?php View::INCLUDE('navbar');?>

	<main>
		<div class="wrap">
          <div class="section">
            <div class="content-wrap">
              <div class="content-header">
                <h2 class="title">Detail Train</h2>
              </div>
              <div class="content-body">
                
              </div>
            </div>
          </div>
          <div class="mask"></div>
        </div>
	</main>

<?php View::HTML_END();?>