<?php

use App\Core\View;
?>

<?php View::HTML_BEGIN();?>

	<?php View::INCLUDE('header');?>

	<?php View::INCLUDE('navbar');?>

	<main>
		<div class="content-wrap">
		    <div class="row justify-content-center">
		        <div class="col-md-6">
		            <div class="content-header">
		                <div>
		                    <a href="projects.html"> 
		                        <div class="fa fa-angle-double-left mr-2"></div>
		                        Back to projects
		                    </a>
		                </div>
		                <h2 class="title">Search Train</h2>
		            </div>

		            <div class="content-body">
		                <form class="mb-5" action="searching" method="POST">
		                    <div class="form-group">
						        <label>Origin</label>
						        <select name="origin" id="origin" class="select-control" title="Select Origin">
						            
						        </select>
						    </div>
						    <div class="form-group">
						        <label>Destination</label>
						        <select name="destination" id="destination" class="select-control" title="Select destination">
						            
						        </select>
						    </div>
						    <div class="form-group">
						        <label>Date</label>
						        <div class="date-control">
						            <input name="date" class="form-control" type="text" id="startDate" />
						            <label class="fa fa-calendar" for="startDate"></label>
						        </div>
						    </div>
						    <div class="form-group">
						        <label>Payment Limit</label>
						        <select name="productCode" class="select-control" title="Choose Time Limit">
						            <option value="WKAI">2 Hours</option>
						            <option value="TKAI">10 Minute</option>
						        </select>
						    </div>
						    <hr/>
						    <input id="token" name="token" type="text" value="<?php echo($data['token']); ?>" hidden>
						    <div class="form-action">
						        <!-- <a class="btn btn-outline-secondary" href="projects.html">Cancel</a> -->
						        <button class="btn btn-primary" type="submit">Search</button>
						    </div>
		                </form>
		            </div>
		        </div>
		    </div>
		</div>
	</main>

	<script type="text/javascript">
	    
	    (function () {

	        getOrigins();

	        getDestinations();
	    })();


	    function getOrigins () {
	        let origin = document.getElementById("origin");
	        
	        const trains = getTrains().data;

	        for (var i = 0; i < trains.length; i++) {
	            let option = document.createElement("OPTION");
	            
	            let optionText = document.createTextNode(trains[i]['nama_stasiun']);

	            option.setAttribute("value", trains[i]['id_stasiun']);
	            option.appendChild(optionText);

	            origin.appendChild(option);
	        }

	        //id_stasiun: "BD", nama_stasiun: "BANDUNG", nama_kota: "BANDUNG",
	    }

	     function getDestinations () {
	        let destination = document.getElementById("destination");
	        
	        const trains = getTrains().data;

	        for (var i = 0; i < trains.length; i++) {
	            let option = document.createElement("OPTION");
	            
	            let optionText = document.createTextNode(trains[i]['nama_stasiun']);

	            option.setAttribute("value", trains[i]['id_stasiun']);
	            option.appendChild(optionText);

	            destination.appendChild(option);
	        }

	        //id_stasiun: "BD", nama_stasiun: "BANDUNG", nama_kota: "BANDUNG",
	    }

	    function getTrains () {
	        const TRAINS = <?php Print($data['station']); ?>;

	        return TRAINS;
	    }
	</script>

<?php View::HTML_END();?>