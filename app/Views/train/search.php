<?php

use App\Core\View;

$trains = $data['trains']['data'];

$iteration = 0; $index = 0;

print_r($data['searching']);
die();

?>

<?php View::HTML_BEGIN();?>

	<?php View::INCLUDE('header');?>

	<?php View::INCLUDE('navbar');?>

	<?php View::BODY_BEGIN();?>

	<main>
		<div class="wrap">
          <div class="section">
            <div class="content-wrap">
              <div class="content-header">
                <h2 class="title">List Train</h2>
              </div>
              <div class="content-body">
                <div class="table-responsive-md">
                  <table class="table table-custom">
                    <thead>
                      <tr>
                        <th rowspan="2" width="1%">No.</th>
                        <th rowspan="2" width="75px">Train Name</th>
                        <th colspan="2">Timeline</th>
                        <th rowspan="2" width="1%">Train Number</th>
                        <th rowspan="2" width="60px">Departure Date</th>
                        <th rowspan="2" width="60px">Arrival Date</th>
                        <th rowspan="2" width="1%"></th>
                      </tr>
                      <tr>
                        <th width="130px">Departure</th>
                        <th width="130px">Arrival</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php

                    		foreach ($trains as $train)
                    		{
                    			echo '<tr>';
									echo '<td align="center">'.   ++$iteration              .'</td>';
									echo '<td >'. 				  $train['trainName']       .'</td>';
									echo '<td align="center">'.   $train['departureTime']   .'</td>';
									echo '<td align="center">'.   $train['arrivalTime']     .'</td>';
									echo '<td align="center">'.   $train['trainNumber']     .'</td>';
									echo '<td align="center">'.   $train['departureDate']   .'</td>';
									echo '<td align="center">'.   $train['arrivalDate']     .'</td>';
									echo '<td align="center">';
										 echo '<div class="dropdown">';
										 	   		echo '<button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">';
										 	   				echo "Action";
													echo '</button>';
													echo '<div class="dropdown-menu dropdown-menu-right">';
														echo '<button type="button" class="dropdown-item" data-toggle="modal" '.'data-target="#modal'.$index.'">';
															echo "Detail";
														echo '</button>';
													echo '</div>';
										 echo '</div>';
									echo '</td>';    
                    			echo '</tr>';

                    			$index++;
                    		}
                    	?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="mask"></div>
        </div>
	</main>
	
	<?php View::BODY_END();?>

	<?php
		$i = 0;

		foreach ($trains as $train) {
			echo '<form action="fare" method="POST" class="modal fade" id="modal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
				echo '<div class="modal-dialog modal-lg" role="document">';
					echo '<div class="modal-content">';
						echo '
							<div class="modal-header">
								<h4 class="modal-title text-left" id="myModalLabel">Detail Train Seats</h4>
								&nbsp;
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						        	<span aria-hidden="true">&times;</span>
						        </button>
						     </div>
						';
						echo '<div class="modal-body">';
							echo '<table class="table table-hover">';
								echo '
									<tr>
				                        <th >No.</th>
				                        <th >Class</th>
				                        <th >Availability</th>
				                        <th >Grade</th>
				                        <th >Price Adult</th>
				                        <th >Price Child</th>
				                        <th >PriceInfant</th>
				                        <th >Action</th>
			                      	</tr>
								';
								if (count($train['seats']) === 1) {
									echo '<tr>';
										echo '<td >'. 1 .'</td>';
										echo '<td >'. $train['seats'][0]['class'] 			.'</td>';
										echo '<td >'. $train['seats'][0]['availability'] 	.'</td>';
										echo '<td >'. $train['seats'][0]['grade'] 			.'</td>';
										echo '<td >'. $train['seats'][0]['priceAdult'] 		.'</td>';
										echo '<td >'. $train['seats'][0]['priceChild'] 		.'</td>';
										echo '<td >'. $train['seats'][0]['priceInfant'] 	.'</td>';
										echo '
											  <td>
												<button type="submit" class="btn btn-primary">Booking</button>
											  </td>
										';
									echo '</tr>';
								} else {
									for ($in = 0; $in < count($train['seats']); $in++) {
										echo '<tr>';
											echo '<td >'. 1 .'</td>';
											echo '<td >'. $train['seats'][$in]['class'] 			.'</td>';
											echo '<td >'. $train['seats'][$in]['availability'] 		.'</td>';
											echo '<td >'. $train['seats'][$in]['grade'] 			.'</td>';
											echo '<td >'. $train['seats'][$in]['priceAdult'] 		.'</td>';
											echo '<td >'. $train['seats'][$in]['priceChild'] 		.'</td>';
											echo '<td >'. $train['seats'][$in]['priceInfant'] 		.'</td>';
											echo '
												  <td>
													<button type="submit" class="btn btn-primary">Booking</button>
												  </td>
											';
										echo '</tr>';
									}
								}
							echo '</table>';
						echo '</div>';

						echo '
							<div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						    </div>
						';
					echo '</div>';
				echo '</div>';
			echo '</form>';
			
			$i++;
		}
	?>

<?php View::HTML_END();?>
