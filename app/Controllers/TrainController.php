<?php

use App\Route\Api;
use App;
use App\Core\Controller;
use App\Jobs\RequestTrait as Request;
use App\Jobs\LoginTrait as Login;
use App\Jobs\ClientTrait as Client;

class TrainController extends Controller
{
	use Login, Client, Request;

	private $credential;

	protected $api;

	public function __construct()
	{
		$this->api = new Api("https://api-dev.fastravel.co.id/train/");
	}

	protected function getCredential()
	{	
		$data = [
			'outletId' 	=> $_REQUEST['outletId'],
			'pin' 		=> $_REQUEST['pin'],
			'key' 		=> $_REQUEST['key'],
		];

		$api = new Api("https://api-dev.fastravel.co.id/app/");

		$body = $this->setCredential($data);

		$this->credential = $this->setUp($api->url, $api->section("sign_in"), $body);
		
		$credential = self::getContentBody($this->credential);

		return $credential;
	}

	public function station()
	{
		$STATION_API = $this->api->section('station');

		$token = json_decode($this->getCredential(), true)['token'];
		
		$station = $this->route("POST", $this->api->url.$STATION_API, ['token' => $token]);
		
		return $this->view('train/station', compact('station', 'token'));
	}

	public function searching()
	{
		$STATION_API = $this->api->section('search');

		$searching = $this->requested('productCode', 'origin', 'destination', 'date', 'token');

		$getTrainList = $this->route("POST", $this->api->url.$STATION_API, [
			'productCode' 	=> $searching['productCode'],
		    'origin' 		=> $searching['origin'],
		    'destination' 	=> $searching['destination'],
		    'date' 			=> $searching['date'],
		    'token' 		=> $searching['token']
		]);

		$trains = json_decode($getTrainList, true);
		/*print_r($trains);
		die();*/
		return $this->view('train/search', compact('trains', 'searching'));
	}


	private static function getContentBody($credential)
	{
		return $credential->getBody()->getContents();
	}
}

