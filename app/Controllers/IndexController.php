<?php 

use App\Core\Controller;

class IndexController extends Controller
{
	public function index()
	{
		return $this->view('login/index');
	}
}