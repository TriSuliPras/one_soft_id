<?php 

namespace App\Jobs;

use GuzzleHttp\Client;

trait ClientTrait
{
	public function setUp($url, $section, $body)
	{
		return $this->initialSetup($url, $section, $body);
	}

	protected function initialSetup($url, $section, $body)
	{
		$client = new Client([
		    'base_uri' => $url,
		    'timeout'  => 5.0,
		    'content-type'  => 'application/json'
		]);

		$credential = $client->request("POST", $section, ['json' => $body]);

		return $credential;
	}

	public function route($method, $url, $params = null)
	{
		return $this->setUpRequest($method, $url, $params);
	}

	protected function setUpRequest($method, $url, $params = null)
	{
		$client = new Client([
		    'base_uri' => $url,
		    'timeout'  => 7.0,
		    'content-type'  => 'application/json'
		]);

		$response = null;

		if ($this->isParamsExist($params)) {

			$response = $client->request(
				$method, $url, ['json' => $params]
			)->getBody()->getContents();
		} 
		else {
			$response = $client = $this->client->request(
				$method, $url
			)->getBody()->getContents();
		}

		return $response;	
	}

	private function isParamsExist($params = null)
	{
		$isParamsExist = false;

		if (! is_null($params)) {

			$isParamsExist = true;
		} 

		return $isParamsExist;
	}
}