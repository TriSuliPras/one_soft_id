<?php

namespace App\Jobs;

trait LoginTrait
{
	protected function setCredential(...$credentials)
	{
		$data = [];

		for ($i = 0; $i < count($credentials); $i++) {
			$credentials = [
				array_keys($credentials)[$i] => array_values($credentials)[$i]
			];

			$data = $credentials[0];
		}

		return $data;
	}
}