<?php

namespace App\Jobs;

trait RequestTrait
{
	public function getting(...$datas)
	{
		
	}

	public function posting(...$datas)
	{

	}

	protected function requested(...$requests)
	{
	    $data = [];
	    
	    foreach ($requests as $key => $request) {
            if ($request === "date") {
                $month = explode('/', $_POST[$request])[0];
                $day = explode('/', $_POST[$request])[1];
                $year = explode('/', $_POST[$request])[2];

                $day = $year.'-'.$month.'-'.$day;

                $data[$request] = $day;
            } else {
                $data[$request] = $_POST[$request];
            }
        }

        return $data;
	}

	protected function isEmpty(...$datas)
	{
		/*$errors = [];
		foreach ($datas) {
			
		}
		if (is_string($data)) {
			if () {

			}
		}*/
	}
}