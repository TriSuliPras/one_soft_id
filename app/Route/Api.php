<?php

namespace App\Route;

class Api
{
	public $url;

	public $page;

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function section($page)
	{
		$this->page = $page;

		return $this->page;
	}
}

return [
	'train' => [
		'sign_in' 	=> "https://api-dev.fastravel.co.id/app/",
		'home'		=> "https://api-dev.fastravel.co.id/train/",
		'page' => [
			'station' 	=> "station",
			'searching' => "search",
			'fared'	  	=> "fare",
			'booking'	=> "book"
		]
	]
];
